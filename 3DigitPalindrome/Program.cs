﻿using System;
using System.Collections.Generic;

namespace _3DigitPalindrome
{
    class Program
    {

        public static bool isPalindrome(string myString)
        {
            string first = myString.Substring(0, myString.Length / 2);
            char[] arr = myString.ToCharArray();

            Array.Reverse(arr);

            string temp = new string(arr);
            string second = temp.Substring(0, temp.Length / 2);

            return first.Equals(second);
        }

        public static void FindLargestPalindrome()
        {
            int largest = 0;
            List<int> numbers = new List<int>();
            for (int i = 999; i > 99; i--)
            {
                for (int j = i; j > 99; j--)
                {
                    int product = i * j;
                    String productString = product.ToString();
                    if (isPalindrome(productString) && product >= largest)
                    {
                        largest = product;
                    };
                }
            }
            Console.WriteLine(largest);
        }

        static void Main(string[] args)
        {
            FindLargestPalindrome();
        }
    }
}
